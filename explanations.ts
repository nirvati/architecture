const html = String.raw;

const explanations = {
  "kube-proxy": html`<h1>kube-proxy</h1>
    <p>
      Kube-proxy is a network proxy that runs on each server. It is responsible
      for network rules and passes through traffic to the appropriate component
      based on IP and port.
    </p>`,
  traefik: html`<h1>Traefik</h1>
    <p>
      Traefik is an HTTP reverse proxy and load balancer. It is responsible for
      routing HTTP requests to the appropriate app. It also handles HTTPS
      encryption and allows to protect services with authentication.
    </p>`,
  "cert-manager": html`<h1>Cert-manager</h1>
    <p>
      Cert-manager is responsible for generating and managing HTTPS certificates
      for Nirvati and apps.
    </p>`,
  core: html`<h1>Nirvati Core</h1>
    <p>
      This is the core of Nirvati. It is for updating system components,
      configuring cert-manager to generate certificates, and manages all other
      Nirvati components.
    </p>`,
  "app-manager": html`<h1>App manager</h1>
    <p>
      The app manager is responsible for installing apps, updating them, and
      removing them. It also manages the various app stores that users can get
      their apps from.
    </p>`,
  "user-interface": html`<h1>User interface</h1>
    <p>
      The user interface is the main web interface of Nirvati. It exposes the
      data from the API server and allows to interact with it in a user-friendly
      way.
    </p>`,
  "api-server": html`<h1>API server</h1>
    <p>
      The API server is responsible for storing information about the various
      apps and their configuration. It is also responsible for managing users
      and their permissions. For installing apps, it instructs the manager to
      perform the installation, and then stores the information about the
      installed app. It also interacts with Longhorn to manage the storage
      volumes for apps.
    </p>`,
  postgresql: html`<h1>PostgreSQL</h1>
    <p>
      PostgreSQL is a relational database. It is responsible for storing the
      data of the API server.
    </p>`,
  longhorn: html`<h1>Longhorn</h1>
    <p>
      Longhorn is a distributed block storage system for Kubernetes. It is
      responsible for storing the data of the apps and Nirvati itself. Longhorn
      makes various features, such as scheduled and manual snapshots or backups,
      as well as distributing the data across multiple servers, possible.
    </p>`,
  "kube-apiserver": html`<h1>kube-apiserver</h1>
    <p>
      Kube-apiserver is the main control plane component of Kubernetes. It
      controls the Kubernetes API and stores the state of the cluster. For
      example, requests to create new apps or storage volumes will end up here.
    </p>`,
  calico: html`<h1>Calico</h1>
    <p>
      Calico is a networking and network security solution for Kubernetes. It is
      responsible for routing traffic between the different components and apps,
      even across different servers.
    </p>`,
  prometheus: html`<h1>Prometheus</h1>
    <p>
      Prometheus is an optional monitoring component. It can be used to monitor
      the health and state of various components and apps and keep track of
      different metrics through time. The data from Prometheus can be visualized
      using other tools, such as Grafana. Prometheus is merely responsible for
      collecting the data.
    </p>`,
  kubewarden: html`<h1>Kubewarden</h1>
    <p>
      Kubewarden allows installing & configuring security policies. These
      policies can be used to restrict access to certain apps or resources, or
      to detect suspicious behavior.
    </p>`,
  tailscale: html`<h1>Tailscale</h1>
    <p>
      Tailscale is an optional VPN service. It can be used to connect to Nirvati
      remotely, for example to access the user interface or apps.
    </p>`,
  "tailscale-controller": html`<h1>Tailscale controller</h1>
    <p>This tool helps the API server to interact with Tailscale.</p>`,
  istio: html`<h1>Istio</h1>
    <p>
      Istio is an optional service mesh. It can be used to manage traffic
      between apps, for example to implement encryption or authentication, as
      well as to monitor the traffic between apps.
    </p>`,
  // The script titles this wrongly, it is actually "Physical hardware"
  storage: html`<h1>Physical hardware</h1>
    <p>
      The physical hardware is the actual server that runs the various
      components and apps. It is responsible for running the apps and storing
      their data. Nirvati also supports running on multiple servers, which
      allows to distribute the load across multiple servers and to make the
      system more fault-tolerant.
    </p>`,
  "operating-system-(linuxfreebsd)": html`<h1>
      Operating system (Linux/FreeBSD)
    </h1>
    <p>
      The operating system is the software that runs on the physical hardware.
      It is responsible for running the various components and apps. Nirvati
      supports running on Linux, and will in the future also support FreeBSD.
    </p>`,
  k3srke2: html`<h1>k3s/RKE2</h1>
    <p>
      K3s and RKE are two different Kubernetes distributions. They are
      responsible for orchestrating the various components and apps. K3s is a
      lightweight Kubernetes distribution that is optimized for running on
      resource-constrained systems, such as Raspberry Pis. RKE2 is a Kubernetes
      distribution that is optimized for running on larger servers, and has a
      focus on security.
    </p>`,
  containerd: html`<h1>Containerd</h1>
    <p>
      Containerd is a container runtime. It is responsible for running the apps
      and managing their lifecycle. However, unlike Kubernetes, it only knows
      about the running containers, while Kubernetes also knows about
      networking, what containers belong to which app, etc.
    </p>`,
  runcrunj: html`<h1>Runc/Runj</h1>
    <p>
      Runc (or Runj for FreeBSD) is a container runtime. It is responsible for
      actually executing the containers. It is used by containerd.
    </p>`,
  "useroem-host-components": html`<h1>User/OEM host components</h1>
    <p>
      The user/OEM host components are the components that are installed on the
      host system by either the user or the device manufacturer. They are
      independent of Nirvati, and often provide additional functionality or
      better hardware support.
    </p>`,
  "web-browsers": html`<h1>Web browsers</h1>
    <p>
      Web browsers are used to access the user interface of Nirvati and the apps
      that are installed on it.
    </p>`,
  "client-apps": html`<h1>Client apps</h1>
    <p>
      Client apps are apps that are installed on the user's computer. They are
      used to access the user interface of Nirvati and the apps that are
      installed on it. Unlike web browsers, they may also access app APIs
      directly instead of going through the user interface.
    </p>`,
  apps: html`<h1>Apps</h1>
    <p>
      Apps are the actual applications that are installed on Nirvati. They are
      responsible for providing the functionality that the user wants to use.
      For example, a user might install a Nextcloud app to store their files in
      the cloud.
    </p>`,
  kubernetes: html`<h1>Kubernetes</h1>
    <p>
      Kubernetes is a set of standard APIs and components for managing
      containerized applications. It is responsible for orchestrating the
      various components and apps. It is implemented by either K3s or RKE on
      Nirvati.
    </p>`,
};

export default explanations as Record<string, string>;
