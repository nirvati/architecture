// CLI util that takes an Excalidraw file and a corresponding SVG file and groups the SVG elements by their Excalidraw group ID.

import fs from "node:fs";

import {
  ExcalidrawElement,
  ExcalidrawTextElement,
} from "@excalidraw/excalidraw/types/element/types";
import slugify from "slugify";
import { ElementNode, Node, parse } from "svg-parser";
import type { Element } from "hast";

import { toHtml } from "hast-util-to-html";
import { optimize } from "svgo";

const excalidrawFile = process.argv[2];
const svgFile = process.argv[3];

const excalidrawData = fs.readFileSync(excalidrawFile, "utf8");
const svgData = fs.readFileSync(svgFile, "utf8");

// First, parse the Excalidraw file.
const excalidrawElements: ExcalidrawElement[] =
  JSON.parse(excalidrawData).elements;

async function main() {
  // Group the elements by their group ID.
  const groupedElements: Record<string, ExcalidrawElement[]> = {};
  for (const element of excalidrawElements) {
    if (element.groupIds.length == 1) {
      for (const groupId of element.groupIds) {
        if (!(groupId in groupedElements)) {
          groupedElements[groupId] = [];
        }
        groupedElements[groupId].push(element);
      }
    } else if (element.groupIds.length == 0) {
      if (!("ungrouped" in groupedElements)) {
        groupedElements["ungrouped"] = [];
      }
      groupedElements["ungrouped"].push(element);
    } else {
      for (const groupId of element.groupIds) {
        if (!(groupId in groupedElements)) {
          groupedElements[groupId] = [];
        }
        groupedElements[groupId].push(element);
      }
    }
  }

  const regroupedElements: Record<string, ExcalidrawElement[]> = {};
  const notRegroupedElements: Record<string, ExcalidrawElement[]> = {};
  // Change each group's ID to the slug of the first text element in the group's value.
  for (const groupId in groupedElements) {
    if (groupId === "ungrouped") {
      regroupedElements[groupId] = groupedElements[groupId];
      continue;
    }
    const group = groupedElements[groupId];
    const textElement = group.find((element) => element.type === "text");
    if (textElement) {
      const text = (textElement as ExcalidrawTextElement).text;
      const newGroupId = slugify(text, { lower: true });
      regroupedElements[newGroupId] = group;
    } else {
      notRegroupedElements[groupId] = group;
    }
  }

  // If an element is both in a group in regroupedElements and in a group in notRegroupedElements, then remove it from the group in notRegroupedElements.
  for (const [id, children] of Object.entries(notRegroupedElements)) {
    const newChildren = children.filter(
      (child) => !Object.values(regroupedElements).flat().includes(child)
    );
    if (newChildren.length === 0) {
      delete notRegroupedElements[id];
    } else {
      notRegroupedElements[id] = newChildren;
    }
  }

  // Now, parse the SVG file.
  const svgContents = parse(svgData);

  const svgElementsByGroupId: Record<string, any[]> = {};
  // For each group, find the corresponding SVG elements and group them together.
  for (const groupId in { ...regroupedElements, ...notRegroupedElements }) {
    const group = regroupedElements[groupId] || notRegroupedElements[groupId];
    // Each group child has an id. Find the SVG elements that have that id as their data-excalidraw-id attribute.
    const svgGroupElements = [];
    for (const element of group) {
      const excalidrawId = element.id;
      const svgElement = (svgContents.children[0] as ElementNode).children.find(
        (element) =>
          (element as Node as ElementNode).properties!["data-excalidraw-id"] ===
          excalidrawId
      );
      if (svgElement) {
        svgGroupElements.push(svgElement);
      }
    }
    if (svgGroupElements.length !== 0) {
      svgElementsByGroupId[groupId] = svgGroupElements;
    }
  }

  //Generate new g elements for each group.
  const newGElements = [];
  for (const groupId in svgElementsByGroupId) {
    const group = svgElementsByGroupId[groupId];
    const newGElement: Element = {
      type: "element",
      tagName: "g",
      properties: {
        id: groupId,
      },
      children: group,
    };
    if (regroupedElements[groupId] && groupId !== "ungrouped") {
      newGElement.properties["class"] = "explanation-item";
    }
    newGElements.push(newGElement);
  }

  // Replace the existing g elements with the new ones.
  (svgContents.children[0] as Node as ElementNode).children = [
    ...(svgContents.children[0] as ElementNode).children.filter(
      (element) => (element as Node as ElementNode).tagName !== "g"
    ),
    ...(newGElements as Node[]),
  ];

  const svg = optimize(toHtml(svgContents as any as Element)
    .replaceAll("<mask></mask>", "")
    .replaceAll("&#x26;amp;", "&amp;"), {
      multipass: true,
      plugins: [
        {
          name: 'preset-default',
          params: {
            overrides: {
              removeViewBox: false,
            }
          }
        },
        {
          name: "convertPathData",
          params: {
            applyTransforms: true,
          }
        },
        {
          name: "removeAttrs",
          params: {
            attrs: ["data-excalidraw-id"],
          },
        }
      ],
    }).data;

  const htmlString = `<!DOCTYPE html>
  <html prefix="og: http://ogp.me/ns#">
  <head>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@runcitadel">
    <meta name="twitter:creator" content="@AaronDewes">
    <meta name="twitter:title" content="The Nirvati architecture">
    <meta name="twitter:description" content="Explore the Nirvati architecture interactively and learn more about how your server actually works.">
    <meta name="twitter:image" content="https://architecture.nirvati.org/card.png">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The Nirvati architecture">
    <meta property="og:url" content="https://architecture.nirvati.org">
    <meta property="og:image" content="https://architecture.nirvati.org/card.png">
    <meta property="og:description" content="Explore the Nirvati architecture interactively and learn more about how your server actually works.">
    <meta name="og:type" content="website">
    <meta name="og:title" content="The Nirvati architecture">
    <meta name="og:url" content="https://architecture.nirvati.org">
    <meta name="og:image" content="https://architecture.nirvati.org/card.png">
    <meta name="og:description" content="Explore the Nirvati architecture interactively and learn more about how your server actually works.">
    <meta name="description" content="Explore the Nirvati architecture interactively and learn more about how your server actually works.">
    <title>What is Nirvati?</title>
    <link rel="stylesheet" href="what-is-nirvati.css">
    <script src="what-is-nirvati.js" defer></script>
  </head>
  <body>${svg}
    <dialog class="popup">
      <div></div>
    </dialog>
  </body>
  </html>`;

  fs.writeFileSync("dist/index.html", htmlString);
}

fs.mkdirSync("dist", { recursive: true });
main();
console.log("Generated index.html.");
await Bun.build({
  entrypoints: ["./what-is-nirvati.ts"],
  outdir: "./dist",
});
console.log("Generated what-is-nirvati.js.");
fs.cpSync("what-is-nirvati.css", "dist/what-is-nirvati.css");
fs.cpSync("card.png", "dist/card.png");
console.log("Copied static files.");
