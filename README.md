# What is Nirvati?

An interactive demo of Nirvati's architecture.

The .svg file is generated using a custom excalidraw fork that adds data-excalidraw-id attributes to the elements.

The code for this fork will be uploaded soon.
