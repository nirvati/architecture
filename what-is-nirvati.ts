/// <reference lib="dom" />

import explanations from "./explanations.js";

const popup = document.querySelector("dialog.popup") as HTMLDialogElement;
const inner = popup?.querySelector("div")!;

function initApp() {
  document.querySelectorAll(".explanation-item").forEach((el) => {
    const elRect = el.getBoundingClientRect();
    // Add an invisible div to make the explanation items clickable
    const invisible = document.createElement("div");
    invisible.className = "invisible";
    invisible.style.top = `${elRect.top + window.scrollY}px`;
    invisible.style.left = `${elRect.left}px`;
    invisible.style.width = `${elRect.width}px`;
    invisible.style.height = `${elRect.height}px`;
    invisible.style.position = "absolute";
    document.body.appendChild(invisible);
    invisible.addEventListener("mouseenter", () => {
      el.classList.add("hover");
    });
    invisible.addEventListener("mouseleave", () => {
      el.classList.remove("hover");
    });
    invisible.addEventListener("click", () => {
      inner.innerHTML = explanations[el.id];
      popup.showModal();
      popup.addEventListener("click", () => {
        popup.close();
      });
      // To make the dialog close on click outside of it, but keep it open when clicking inside
      inner.addEventListener("click", (e) => {
        e.stopPropagation();
      });
    });
  });
}

// On resize, remove the invisible divs and re-add them
window.addEventListener("resize", () => {
  document.querySelectorAll(".invisible").forEach((el) => {
    el.remove();
  });
  initApp();
});

initApp();
